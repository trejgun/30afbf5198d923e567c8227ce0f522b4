Swift Coin Asia
===============

## Pre installation

mac
```bash
sudo port install nodejs8 npm5 mongodb mongodb-tools redis
```


To make the first build run:

clone repo
```bash
git clone git@bitbucket.org:trejgun/30afbf5198d923e567c8227ce0f522b4.git
```

install
```bash
# set mongodb user/pass (if any) in 
nano ./server/shared/configs/mongo.js

# set twitter keys in
nano ./server/shared/configs/config.js

# set cwd (path to project) in
nano ./ecosystem.json

npm i
```

run
```bash
npm start
```

test
```bash
npm t
```

eslint
```bash
npm run lint
```
