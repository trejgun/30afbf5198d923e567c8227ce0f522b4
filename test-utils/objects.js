import {getRandomFloat, getRandomString} from "../server/shared/utils/misc";
import {
    companyName,
    confirm,
    country,
    domainName,
    email,
    fullName,
    password,
    phoneNumber,
    systemId
} from "../shared/constants/test";
import {defaultLanguage as language} from "../shared/constants/language";


export function makeTestEmail(type) {
	const parts = email.split("@");
	return `${parts[0]}+${type}+${getRandomString(10).toLowerCase()}@${parts[1]}`;
}

export function makeTestDomainName() {
	return `${getRandomString(10)}.${domainName}`;
}


export function organizationObject(...data) {
	return Object.assign({
		companyName,
		domainName: makeTestDomainName(),
		phoneNumber,
		organizations: [systemId],
		email: makeTestEmail("organization"),
		delivery: true,
		address: "very long street 123",
		coordinates: [getRandomFloat(-180, 180, 5), getRandomFloat(-90, 90, 5)]
	}, ...data);
}

export function userObject(...data) {
	return Object.assign({
		password,
		confirm,
		fullName,
		phoneNumber,
		language,
		country,
		isEmailVerified: true,
		email: makeTestEmail("user")
	}, ...data);
}
