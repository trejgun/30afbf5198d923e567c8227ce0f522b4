import {getRandomFloat, getRandomString} from "../server/shared/utils/misc";
import populate from "./populate";
import {
    organizationObject,
    userObject
} from "./objects";

import UserController from "../server/main/controllers/user";
import OrganizationController from "../server/main/controllers/organization";


export function createOrganization(...args) {
	const organizationController = new OrganizationController();
	return organizationController.create(populate(...args, (organization) =>
		organizationObject({
			companyName: getRandomString(getRandomFloat(5, 16), 2).toLowerCase()
		}, organization)));
}

export function createUser(...args) {
	const userController = new UserController();
	return userController.create(populate(...args, (user, nested) =>
		userObject({
			organizations: [].concat(nested.Organization)
		}, user)));
}
