import React, {Component} from "react";
import PropTypes from "prop-types";
import {Grid} from "react-bootstrap";


export default class Article extends Component {
	static propTypes = {
		children: PropTypes.element
	};

	render() {
		// console.log("Article:render", this.props, this.state);
		return (
			<Grid>
				{this.props.children}
			</Grid>
		);
	}
}
