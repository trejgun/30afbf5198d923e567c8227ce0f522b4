import "./main/styles.less";
import "../static/favicon.ico";
import App from "./main/app";
import hydrate from "./shared/utils/hydrate";
import configureStore from "./main/store";


process.env.MODULE = "main";

const store = configureStore(window.__INITIAL_STATE__);

hydrate(App, store);

if (module.hot) {
	module.hot.accept("./main/app", () => {
		hydrate(App, store);
	});
}
