import {replaceAll} from "../../shared/utils/reducer";

const settings = {
	list: [],
	count: 1,
	isLoading: true,
	success: false,
	name: "view"
};

export default function settingsReducer(state = settings, action) {
	switch (action.type) {
		case "tweets_list_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name
			});
		case "tweets_list_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceAll(state, action.data.list),
				count: action.data.count,
				name: action.name
			});
		case "tweets_list_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name
			});
		default:
			return state;
	}
}
