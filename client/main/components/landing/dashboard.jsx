import React, {Component} from "react";
import {Col, Row} from "react-bootstrap";
import withListHelper from "../../../shared/components/forms/withListHelper";
import Form from "./dashboard.form";
import List from "./dashboard.list";


@withListHelper("tweets")
export default class Dashboard extends Component {
    render() {
        // console.log("Dashboard:render", this.props);
        return (
            <Row>
                <Col>
                    <Form {...this.props}/>
                    <br/>
                    <List {...this.props} limit={100}/>
                </Col>
            </Row>
        );
    }
}
