import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Button} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withListFormHelper from "../../../shared/components/forms/withListFormHelper";


@withListFormHelper("bookings")
export default class DashboardForm extends Component {

    static propTypes = {
        setState: PropTypes.func,
        onChange: PropTypes.func,
        onSubmit: PropTypes.func,
        storeName: PropTypes.string,
        currency: PropTypes.string,
        value: PropTypes.number,
        calc: PropTypes.object
    };

    state = {};

    componentDidMount() {
        this.props.onSubmit();
    }

    render() {
        console.log("DashboardForm:render", this.props);

        return (
            <Form onSubmit={this.props.onSubmit}>
                <Button type="submit">
                    <FormattedMessage id="components.buttons.refresh" />
                </Button>
            </Form>
        );
    }
}
