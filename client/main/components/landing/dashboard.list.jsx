import React, {Component} from "react";
import PropTypes from "prop-types";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import withListItemHelper from "../../../shared/components/forms/withListItemHelper";


@withListItemHelper("tweets", ListGroup)
export default class DashboardListItem extends Component {

    static propTypes = {
        text: PropTypes.string
    };

    render() {
        // console.log("DashboardListItem:render", this.props);
        return (
            <ListGroupItem>
                {this.props.text}
            </ListGroupItem>
        );
    }
}
