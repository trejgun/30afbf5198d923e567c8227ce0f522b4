import TwitterAPI from "../../shared/connect/twitter";


export default class TwitterController {
    static getTweets() {
        return TwitterAPI.searchTweets("statuses/user_timeline", {count: 100})
            .then(({data}) => ({list: data}));
    }
}
