import bluebird from "bluebird";
import {pick} from "lodash";
import StatefulController from "../../shared/controllers/stateful";
import OrganizationController from "./organization";
import {getConnections} from "../../shared/utils/mongoose";


export default class UserController extends StatefulController {
	static realm = "main";


	sync(request) {
		return bluebird.resolve(request.isAuthenticated() ? request.user : null);
	}

    insert(request) {
        const clean = pick(request.body, ["country", "confirm", "email", "fullName", "image", "language", "password", "social"]);
        Object.assign(clean, {
            isEmailVerified: request.params.isEmailVerified
        });
        console.log("insert", clean);
        return this.create(clean)
            .then(user => {
                const organizationController = new OrganizationController();
                return organizationController.create({
                    // email: user.email,
                    companyName: user.fullName
                })
                    .then(organization =>
                        this.save(Object.assign(user, {organization}))
                    );
            });
    }

    getByQuery(query, {populate = [], ...options} = {}) {
        const connections = getConnections();
        Object.assign(options, {
            populate: [{
                path: "organizations",
                model: connections.main.model("Organization")
            }].concat(populate)
        });
        return this.findOne(query, options);
    }

	static logout(request, response) {
		request.session.destroy();
		request.logout();
		response.clearCookie();
		return bluebird.resolve({success: true});
	}
}
