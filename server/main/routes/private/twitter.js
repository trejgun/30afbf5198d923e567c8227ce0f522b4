import {Router} from "express";
import {wrapJSON} from "../../../shared/utils/wrapper";
import {methodNotAllowed} from "../../../shared/utils/middleware";
import TwitterController from "../../controllers/twitter";

const router = Router(); // eslint-disable-line new-cap

router.route("/tweets")
	.get(wrapJSON(TwitterController.getTweets))
	.all(methodNotAllowed);

export default router;
