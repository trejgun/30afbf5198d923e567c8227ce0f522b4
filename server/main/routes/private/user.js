import {Router} from "express";
import {wrapJSON} from "../../../shared/utils/wrapper";
import {methodNotAllowed} from "../../../shared/utils/middleware";
import UserController from "../../controllers/user";

const router = Router(); // eslint-disable-line new-cap
const userController = new UserController();

router.route("/sync")
	.get(wrapJSON(::userController.sync))
	.all(methodNotAllowed);

export default router;
