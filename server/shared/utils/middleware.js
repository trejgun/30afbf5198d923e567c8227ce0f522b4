import {makeError} from "./error";


export function methodNotAllowed(request, response, next) {
	return next(makeError("method-not-allowed", 405));
}

export function requiresLogin(request, response, next) {
	if (request.isUnauthenticated()) {
		return next(makeError("login", 401));
	}
	return next();
}
