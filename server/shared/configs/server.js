const config = {
    development: {
        // oauth2 doesn't work with self-signed certificate therefore no https
        // amazone s3 doesn't work with localhost subdomains therefore no *.localhost
        main: {
            protocol: "http",
            hostname: "localhost",
            port: 9000
        }
    }
};

config.test = config.development;
config.staging = config.development;
config.production = config.development;

export default config;
