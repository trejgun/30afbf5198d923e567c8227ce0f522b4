import server from "./server";
import {formatUrl} from "../../../shared/utils/misc";

export default {
	[process.env.NODE_ENV]: {
		rendering: "server",
		createUser: true,
		redis: {
			port: 6379,
			host: "localhost"
		},
		strategies: {
			twitter: {
                consumerKey: "",
                consumerSecret: "",
                callbackURL: `${formatUrl(server[process.env.NODE_ENV][process.env.MODULE])}/api/auth/twitter/callback`
			}
		},
		session: {
			proxy: false,
			secret: "keyboard_cat",
			saveUninitialized: true,
			resave: false,
			rolling: true,
			name: "ManageSocial",
			cookie: {
				path: "/",
				httpOnly: true,
				secure: false,
				maxAge: 24 * 60 * 60 * 1000,
				signed: false
			}
		},
		twitter: {
            consumer_key: "", // eslint-disable-line camelcase
            consumer_secret: "", // eslint-disable-line camelcase
            access_token: "", // eslint-disable-line camelcase
            access_token_secret: "" // eslint-disable-line camelcase
		}
	}
};
