const config = {
	development: {
        main: {
			url: "localhost",
			user: "",
			pass: ""
		}
	}
};

config.test = config.development;
config.staging = config.development;
config.production = config.development;

export default ["main"].reduce((memo, module) =>
	Object.assign(memo, {
		[module]: {
			url: `mongodb://${config[process.env.NODE_ENV][module].url}/ManageSocial-${process.env.NODE_ENV}-${module}`,
			options: {
				server: {
					socketOptions: {
						autoReconnect: true,
						keepAlive: 300000,
						connectTimeoutMS: 30000
					}
				},
				user: config[process.env.NODE_ENV][module].user,
				pass: config[process.env.NODE_ENV][module].pass
			}
		}
	}), {});
