import {Router} from "express";
import session from "express-session";
import connectRedis from "connect-redis";
import redis from "./redis";
import configs from "../../shared/configs/config";


const router = Router(); // eslint-disable-line new-cap

const config = configs[process.env.NODE_ENV];

router.use(session(Object.assign({}, config.session, {
	store: new (connectRedis(session))(Object.assign(config.redis, {client: redis()}))
})));

export default router;

