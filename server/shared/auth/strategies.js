import passport from "passport";
import {Strategy as TwitterStrategy} from "passport-twitter";
import configs from "../../shared/configs/config";
import {getRandomString} from "../utils/misc";

import UserController from "../../main/controllers/user";


const config = configs[process.env.NODE_ENV];

function createUser(provider, profile) {
	const random = getRandomString(16);
	const userController = new UserController();
	return userController.insert({
		params: {
			isEmailVerified: true
		},
		body: {
			// email: profile.emails[0].value,
			fullName: profile.displayName || `${profile.name.givenName} ${profile.name.familyName}`,
			image: profile.photos[0].value,
			// language: profile._json.language || profile._json.locale.split("_")[0],
			confirm: random,
			password: random,
			social: {
				[provider]: profile.id
			}
		}
	});
}

function getUser(provider, profile) {
	const userController = new UserController();
	return userController.findOne({
		[`social.${provider}`]: profile.id
	}, {lean: false})
		.then(user => {
			if (!user) {
				return createUser(provider, profile);
			}
		});
}


if ("twitter" in config.strategies) {
	passport.use(new TwitterStrategy(config.strategies.twitter,
		(accessToken, refreshToken, profile, callback) => {
			getUser("twitter", profile)
				.tap(user => {
					callback(null, user);
				})
				.catch(error => {
					callback(error, null);
				})
				.done();
		}
	));
}

