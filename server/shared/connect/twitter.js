import Twitter from "twit";
import {decorate, override} from "core-decorators";
import {promise} from "./abstract/decorators";
import AbstractAPI from "./abstract/abstract";
import configs from "../../shared/configs/config";


export default new class TwitterAPI extends AbstractAPI {
	static key = "TWITTER_API";

	@override
	get client() {
        const config = configs[process.env.NODE_ENV];
		return new Twitter(config.twitter);
	}

	@decorate(promise)
	searchTweets(path, message) {
		return this.client.get(path, message);
	}
}();
