export const phoneNumber = "+62 (812) 3919-8760";

export const seed = "ManageSocial";
export const systemId = "ffffffffffffffffffffffff";

export const fullNameMinLength = 2;
export const fullNameMaxLength = 64;
export const fullName = "Trej Gun";

export const companyNameMinLength = 2;
export const companyNameMaxLength = 64;
export const companyName = "Manage Social";

export const domainNameMinLength = 2;
export const domainNameMaxLength = 128;
export const domainName = "ManageSocial.com";

export const descriptionMinLength = 200;
export const descriptionMaxLength = 10000;

export const email = `no-reply@${domainName}`;
